# Quicklaunch


This is a minimally modified version of a combination of the official **`panel-launchers@cinnamon.org`** applets in Linux Mint 19.2 (Cinnamon 4.2) and Mint 21.2 (Cinnamon 5.8), respectively.    

Main feature is the addition of multiline capability, allowing the "quicklaunch" section to shrink as much as possible depending on panel height (or width, for vertical panels). For those who use a large panel - with or without autohide turned on - in conjunction with a traditional window list style (non-grouped taskbar/panel buttons) it would be a major improvement.

![](Screenshot_from_2023-08-16_16-35-54_crop.png)

There can be more instances of the Quicklaunch applet enabled at the same time, which is also true for the original panel-launchers applet. They can even be combined.

![](Screenshot_from_2023-08-16_16-49-23_crop.png)

Problem is, none of the currently existing Menu applets allows the user to select to which panel, and to which of the panel-launchers they would add a "shortcut"/launcher when selecting such option in any application's context menu.

Fortunately, I have found a solution to that issue too, by slightly modifying the official Menu applet in Mint 19.2 (and probably soon applying the same to Mint 21.2's Menu applet). Check out the YAWN Menu applet here in the applets group.

I wonder why the official Mint team didn't bother to implement these two options so far considering it took me somewhere around 3-4 days to figure both out and implement them, working alone on foreign code.


_Drugwash, August 2023_
